Description: Fix font height calculation with newer Pango
Origin: upstream, https://github.com/hexchat/hexchat/pull/2500
Bug-Debian: https://bugs.debian.org/956633

From 86ae618cfa7985de70b42b682cb93a4f4f07bd90 Mon Sep 17 00:00:00 2001
From: John Levon <levon@movementarian.org>
Date: Thu, 3 Sep 2020 23:40:52 +0100
Subject: [PATCH 1/2] Use pango_font_metrics_get_height() to calculate font
 height

With newer versions of Pango and some fonts, the bottom of the
text is clipped (making underscores invisible). Use the correct
calculation when needed to avoid this.

Based on an original fix by Jeff Teunissen.

Closes #2449.
---
 src/fe-gtk/xtext.c | 15 +++++++++++++--
 1 file changed, 13 insertions(+), 2 deletions(-)

--- a/src/fe-gtk/xtext.c
+++ b/src/fe-gtk/xtext.c
@@ -283,8 +283,24 @@
 	metrics = pango_context_get_metrics (context, xtext->font->font, lang);
 	xtext->font->ascent = pango_font_metrics_get_ascent (metrics) / PANGO_SCALE;
 	xtext->font->descent = pango_font_metrics_get_descent (metrics) / PANGO_SCALE;
+
+	/*
+	 * In later versions of pango, a font's height should be calculated like
+	 * this to account for line gap; a typical symptom of not doing so is
+	 * cutting off the underscore on some fonts.
+	 */
+#if PANGO_VERSION_CHECK(1, 44, 0)
+	xtext->fontsize = pango_font_metrics_get_height (metrics) / PANGO_SCALE + 1;
+
+	if (xtext->fontsize == 0)
+		xtext->fontsize = xtext->font->ascent + xtext->font->descent;
+#else
+	xtext->fontsize = xtext->font->ascent + xtext->font->descent;
+#endif
+
 	pango_font_metrics_unref (metrics);
 }
+
 static int
 backend_get_text_width_emph (GtkXText *xtext, guchar *str, int len, int emphasis)
 {
@@ -3479,8 +3495,6 @@
 	if (xtext->font == NULL)
 		return FALSE;
 
-	xtext->fontsize = xtext->font->ascent + xtext->font->descent;
-
 	{
 		char *time_str;
 		int stamp_size = xtext_get_stamp_str (time(0), &time_str);
